SoftConstruct Hello World
=========================

A "Hello World" application for a certain software company.

Launching
---------

* Ensure that Erlang/OTP 21 or greater is installed and on PATH
* Ensure that Rebar 3 is on PATH
* Edit `env/local.config` and change `host` and `port` according to your environment
* Launch Rebar shell:
```
$ rebar3 shell
```
