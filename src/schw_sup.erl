%%%-------------------------------------------------------------------
%% @doc schw top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(schw_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: #{id => Id, start => {M, F, A}}
%% Optional keys are restart, shutdown, type, modules.
%% Before OTP 18 tuples must be used to specify a child. e.g.
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Host = application:get_env(schw, host, "localhost"),
    Port = application:get_env(schw, port, 9222),
    {ok, {
        {one_for_all, 0, 1}, [
            #{
                id => schw_storage_srv,
                start => {schw_storage_srv, start_link, []}
            },
            #{
                id => schw_connector_srv,
                start => {schw_connector_srv, start_link, [Host, Port]}
            }
        ]
    }}.

%%====================================================================
%% Internal functions
%%====================================================================
