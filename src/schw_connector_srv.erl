-module(schw_connector_srv).
-author("co2").

-behaviour(gen_server).

-include_lib("kernel/include/logger.hrl").

%% API
-export([start_link/2]).

%% gen_server callbacks
-export([init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3]).

-define(SERVER, ?MODULE).

-record(schw_connector_srv, {
    page_id,
    server_connection,
    timer
}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link(binary(), non_neg_integer()) ->
    {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link(Host, Port) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, {Host, Port}, []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
    {ok, State :: #schw_connector_srv{}} | {ok, State :: #schw_connector_srv{}, timeout() | hibernate} |
    {stop, Reason :: term()} | ignore).
init({Host, Port}) ->
    FindCoinbasePageIdOrUndefined = fun(#{
        <<"url">> := Url,
        <<"id">> := PageId
    }, undefined) ->
        case binary:part(Url, 0, min(byte_size(Url), 31)) of
            <<"https://pro.coinbase.com/trade/">> -> PageId;
            _ -> undefined
        end;
        (_, Acc) -> Acc
    end,

    try
        {ok, {_, _, Res}} = httpc:request("http://" ++ Host ++ ":" ++ integer_to_list(Port) ++ "/json"),
        case lists:foldl(FindCoinbasePageIdOrUndefined, undefined, jsx:decode(list_to_binary(Res), [return_maps])) of
            undefined ->
                ?LOG_ERROR("Could not find a tab with Coinbase open in it"),
                {stop, {error, coinbase_not_found}};
            PageId ->
                {ok, Conn} = gun:open(Host, Port),
                _ = gun:ws_upgrade(Conn, <<"/devtools/page/", PageId/binary>>),
                {ok, #schw_connector_srv{page_id = PageId, server_connection = Conn}}
        end
    catch C:E:S ->
        ?LOG_ERROR("Error while attempting to start DevTools connector: ~p~n", [{C, E}]),
        {stop, {C, E, S}}
    end.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #schw_connector_srv{}) ->
    {reply, Reply :: term(), NewState :: #schw_connector_srv{}} |
    {reply, Reply :: term(), NewState :: #schw_connector_srv{}, timeout() | hibernate} |
    {noreply, NewState :: #schw_connector_srv{}} |
    {noreply, NewState :: #schw_connector_srv{}, timeout() | hibernate} |
    {stop, Reason :: term(), Reply :: term(), NewState :: #schw_connector_srv{}} |
    {stop, Reason :: term(), NewState :: #schw_connector_srv{}}).
handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #schw_connector_srv{}) ->
    {noreply, NewState :: #schw_connector_srv{}} |
    {noreply, NewState :: #schw_connector_srv{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #schw_connector_srv{}}).
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #schw_connector_srv{}) ->
    {noreply, NewState :: #schw_connector_srv{}} |
    {noreply, NewState :: #schw_connector_srv{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #schw_connector_srv{}}).
handle_info(ping_ws, #schw_connector_srv{server_connection = C} = State) ->
    ok = gun:ws_send(C, ping),
    {noreply, State};
handle_info({gun_ws, C, _, {_, Content}}, #schw_connector_srv{server_connection = C} = State) ->
    DecodedFrame = jsx:decode(Content, [return_maps]),
    Method = maps:get(<<"method">>, DecodedFrame, undefined),
    _ = handle_devtools(Method, DecodedFrame),
    {noreply, State};
handle_info({gun_upgrade, C, _, [<<"websocket">>], _}, #schw_connector_srv{server_connection = C} = State) ->
    ok = gun:ws_send(C, {text, <<"{\"id\":1,\"method\":\"Network.enable\",\"params\":{\"maxPostDataSize\":65536}}">>}),
    ?LOG_INFO("Connected to DevTools!~n", []),
    {ok, TRef} = timer:send_interval(30000, ping_ws),
    {noreply, State#schw_connector_srv{timer = TRef}};
handle_info({gun_response, C, _, _, _, _}, #schw_connector_srv{server_connection = C}) ->
    {stop, {error, ws_upgrade_failed}};
handle_info({gun_error, C, _, _}, #schw_connector_srv{server_connection = C}) ->
    {stop, {error, ws_upgrade_failed}};
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #schw_connector_srv{}) -> term()).
terminate(_Reason, #schw_connector_srv{timer = T}) ->
    case T of
        undefined -> noop;
        T -> timer:cancel(T)
    end,
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #schw_connector_srv{},
    Extra :: term()) ->
    {ok, NewState :: #schw_connector_srv{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

handle_devtools(<<"Network.webSocketFrameReceived">>, #{<<"params">> := #{
    <<"response">> := #{
        <<"payloadData">> := Payload
    }
}}) ->
    #{<<"type">> := Type} = CoinbaseMessage = jsx:decode(Payload, [return_maps]),
    _ = handle_coinbase(Type, CoinbaseMessage),
    ok;
handle_devtools(_, _) -> ok.

handle_coinbase(<<"ticker">>, #{<<"product_id">> := ProductId, <<"price">> := Price}) ->
    ok = schw_storage_srv:update_price(ProductId, binary_to_float(Price));
handle_coinbase(_, _) -> ok.