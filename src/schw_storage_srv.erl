-module(schw_storage_srv).
-author("co2").

-behaviour(gen_server).

-include_lib("kernel/include/logger.hrl").


%% API
-export([start_link/0, update_price/2]).

%% gen_server callbacks
-export([init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3]).

-define(SERVER, ?MODULE).

-record(schw_storage_srv, {
    table_ref
}).

%%%===================================================================
%%% API
%%%===================================================================

update_price(ProductId, Price) ->
    gen_server:call(?MODULE, {update_price, ProductId, Price}).

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
    {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
    {ok, State :: #schw_storage_srv{}} | {ok, State :: #schw_storage_srv{}, timeout() | hibernate} |
    {stop, Reason :: term()} | ignore).
init([]) ->
    T = ets:new(?MODULE, [set]),
    {ok, #schw_storage_srv{table_ref = T}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #schw_storage_srv{}) ->
    {reply, Reply :: term(), NewState :: #schw_storage_srv{}} |
    {reply, Reply :: term(), NewState :: #schw_storage_srv{}, timeout() | hibernate} |
    {noreply, NewState :: #schw_storage_srv{}} |
    {noreply, NewState :: #schw_storage_srv{}, timeout() | hibernate} |
    {stop, Reason :: term(), Reply :: term(), NewState :: #schw_storage_srv{}} |
    {stop, Reason :: term(), NewState :: #schw_storage_srv{}}).
handle_call({update_price, ProductId, Price}, _, #schw_storage_srv{table_ref = T} = State) ->
    true = ets:insert(T, {ProductId, Price}),
    ?LOG_INFO("~p: ~p~n", [ProductId, Price]),
    {reply, ok, State};
handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #schw_storage_srv{}) ->
    {noreply, NewState :: #schw_storage_srv{}} |
    {noreply, NewState :: #schw_storage_srv{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #schw_storage_srv{}}).
handle_cast(_Request, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #schw_storage_srv{}) ->
    {noreply, NewState :: #schw_storage_srv{}} |
    {noreply, NewState :: #schw_storage_srv{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #schw_storage_srv{}}).
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #schw_storage_srv{}) -> term()).
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #schw_storage_srv{},
    Extra :: term()) ->
    {ok, NewState :: #schw_storage_srv{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
